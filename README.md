# STRV
---
## Eventio

Development:
```
$ yarn
$ yarn start
```

Production:
```
$ yarn build
$ yarn serve:prod
```
