export const AUTH = {
    login: 'auth/native'
}

export const EVENTS = {
    attendEvent: id => `events/${id}/attendees/me`,
    createEvent: 'events',
    deleteEvent: id => `events/${id}`,
    getSpecificEvent: id => `events/${id}`,
    listAllEvents: 'events',
    updateEvent: id => `events/${id}`
}
