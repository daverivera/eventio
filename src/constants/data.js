export const VIEW_SELECTORS = { grid: 1, list: 2 }

export const EVENT_CLASSIFICATIONS = {
    ALL: {
        label: 'ALL EVENTS',
        value: 0
    },
    FUTURE:  {
        label: 'FUTURE EVENTS',
        value: 1
    },
    PAST: {
        label: 'PAST EVENTS',
        value: 2
    }
}

export const EVENT_BUTTON_MESSAGE = {
    attendee: 'LEAVE',
    owner: 'EDIT',
    unregistered: 'JOIN'
}

export const EVENT_DATE_FORMAT = 'MMMM DD, YYYY - h:mm a'
