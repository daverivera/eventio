export default {
    eventDetail: eventId => `/events/${eventId}`,
    events: '/events',
    login: '/',
    profile: '/profile'
}
