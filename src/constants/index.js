import * as CONFIGURATION from './configuration'
import * as DATA from './data'
import * as ENDPOINTS from './endpoints'
import * as MESSAGES from './messages'
import PAGES from './pages'

export {
    CONFIGURATION,
    DATA,
    ENDPOINTS,
    MESSAGES,
    PAGES
}
