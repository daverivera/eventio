export const LOGIN_PAGE = {
    email: {
        empty: 'Enter an email'
    },
    failedLogin: 'Oops! That email and password combination is not valid.',
    password: {
        empty: 'Enter a password'
    }
}

export const CREATE_EVENT = {
    capacity: {
        empty: 'Capacity has to be filled up'
    },
    date: {
        empty: 'Date has to be filled up',
    },
    description: {
        empty: 'Description has to be filled up',
    },
    time: {
        empty: 'Time has to be filled up',
    },
    title: {
        empty: 'Title has to be filled up'
    }
}
