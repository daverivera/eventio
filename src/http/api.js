// @vendors
import axios from 'axios'

// @components
import { CONFIGURATION } from '../constants'

const baseURL = CONFIGURATION.SERVER.production
// const baseURL = process.env.NODE_ENV === 'production'
    // ? CONFIGURATION.SERVER.production
    // : CONFIGURATION.SERVER.mock

axios.defaults.baseURL = baseURL
axios.defaults.headers.common['APIKey'] = CONFIGURATION.API_KEY
axios.defaults.headers.post['Content-Type'] = 'application/json'
