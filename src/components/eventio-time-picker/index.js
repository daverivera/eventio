// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { TimePicker } from 'react-md';

// @styles
import './styles.scss'

const EventioTimePicker = ({ className, ...props }) =>
    (
        <TimePicker
            className={classNames('eventio-time-picker', className)}
            {...props}
        />
    )

EventioTimePicker.propTypes = { className: PropTypes.string }

EventioTimePicker.defaultProps = { className: '' }

export default EventioTimePicker
