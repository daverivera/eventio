// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'

// @components
import EventioLogo from '../eventio-logo'
import EventioUserMenu from '../eventio-user-menu'

// @styles
import './styles.scss'

const EventioHeader = ({ onLogoutSession, onOpenProfile, user }) =>
    (
        <div className="eventio-header">
            <EventioLogo />
            <EventioUserMenu
                firstName={user.get('firstName')}
                lastName={user.get('lastName')}
                onLogoutSession={onLogoutSession}
                onOpenProfile={onOpenProfile}
            />
        </div>
    )

EventioHeader.propTypes = {
    onLogoutSession: PropTypes.func.isRequired,
    onOpenProfile: PropTypes.func.isRequired,
    user: ImmutablePropTypes.mapContains({
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired
    }).isRequired
}

export default EventioHeader
