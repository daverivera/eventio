// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'react-md'

// @styles
import './styles.scss'

const EventioCircularButton = ({
    disabled,
    isVisible,
    onClick,
    primary
}) =>
    (
        <Button
            className="eventio-circular-button"
            disabled={disabled}
            icon
            onClick={onClick}
            primary={primary}
            secondary={!primary}
            swapTheming
        >
            {
                primary ? 'add' : 'check'
            }
        </Button>
    )

EventioCircularButton.propTypes = {
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    primary: PropTypes.bool
}

EventioCircularButton.defaultProps = {
    disabled: false,
    primary: true
}

export default EventioCircularButton
