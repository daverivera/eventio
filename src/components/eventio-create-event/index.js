// @vendors
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import {
    DialogContainer,
    FontIcon,
    Paper,
    Toolbar
} from 'react-md'

// @components
import EventioButton from '../eventio-button'
import EventioCirclarButton from '../eventio-circular-button'
import EventioLogo from '../eventio-logo'
import EventioTextField from '../eventio-text-field'
import EventioTimePicker from '../eventio-time-picker'
import EventioDatePicker from '../eventio-date-picker'
import { MESSAGES } from '../../constants'

// @styles
import './styles.scss'

class EventioCreateEvent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            form: {
                capacity: '',
                date: '',
                description: '',
                time: '',
                title: ''
            },
            isButtonDisabled: true,
            isFormVisible: false
        }

        this._displayForm = this._displayForm.bind(this)
        this._hideForm = this._hideForm.bind(this)
        this._updateField = this._updateField.bind(this)
        this._createEvent = this._createEvent.bind(this)
    }

    _createEvent() {
        const { onCreateEvent } = this.props
        const { form } = this.state

        onCreateEvent(form)
    }

    _displayForm() {
        this.setState({ isFormVisible: true })
    }

    _hideForm() {
        this.setState({ isFormVisible: false })
    }

    _updateField(field, value) {
        const { form: formData } = this.state

        const form = Object.assign({}, formData, { [field]: value })
        const isButtonDisabled = !!Object
            .values(form)
            .filter(field => !field)
            .length

        this.setState({
            form,
            isButtonDisabled
        })
    }

    render() {
        const {
            _createEvent,
            _displayForm,
            _hideForm,
            _updateField
        } = this
        const { isButtonDisabled, isFormVisible } = this.state

        return (
            <Fragment>
                <DialogContainer
                    className="eventio-create-event"
                    fullPage
                    id="eventio-create-event"
                    visible={isFormVisible}
                    focusOnMount={false}
                    aria-labelledby="simple-full-page-dialog-title"
                >
                    <Toolbar
                        title={<EventioLogo />}
                        actions={
                            <FontIcon
                                className="eventio-create-event__close-button"
                                onClick={_hideForm}
                            >
                                clear
                            </FontIcon>
                        }
                    />
                    <Paper className="eventio-create-event__content">
                        <h2 className="eventio-create-event__title">
                            Create new event
                        </h2>
                        <h3 className="eventio-create-event__subtitle">
                            Enter details below.
                        </h3>
                        <div className="eventio-create-event__form">
                            <EventioTextField
                                errorText={MESSAGES.CREATE_EVENT.title.empty}
                                id="eventio-create-event-title"
                                onChange={(value) => _updateField('title', value)}
                                placeholder="Title"
                                required
                            />
                            <EventioTextField
                                errorText={MESSAGES.CREATE_EVENT.description.empty}
                                id="eventio-create-event-description"
                                onChange={(value) => _updateField('description', value)}
                                placeholder="Description"
                                required
                            />
                            <EventioDatePicker
                                errorText={MESSAGES.CREATE_EVENT.date.empty}
                                id="eventio-create-event-date"
                                onChange={(value) => _updateField('date', value)}
                                placeholder="Date"
                                required
                            />
                            <EventioTimePicker
                                errorText={MESSAGES.CREATE_EVENT.time.empty}
                                id="eventio-create-event-time"
                                onChange={(value) => _updateField('time', value)}
                                placeholder="Time"
                                required
                            />
                            <EventioTextField
                                errorText={MESSAGES.CREATE_EVENT.capacity.empty}
                                id="eventio-create-event-capacity"
                                onChange={(value) => _updateField('capacity', value)}
                                placeholder="Capacity"
                                required
                                type="number"
                            />
                            <EventioButton
                                className="eventio-create-event__submit-button"
                                disabled={isButtonDisabled}
                                onClick={_createEvent}
                                primary
                            >
                                CREATE NEW EVENT
                            </EventioButton>
                        </div>
                    </Paper>
                </DialogContainer>

                <EventioCirclarButton
                    onClick={_displayForm}
                />
            </Fragment>
        )
    }
}

EventioCreateEvent.propTypes = {
    onCreateEvent: PropTypes.func.isRequired
}

export default EventioCreateEvent
