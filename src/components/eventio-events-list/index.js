// @vendors
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import { bem } from 'react-md'

// @components
import EventioEvent from '../eventio-event'
import EventioListViewSelector from '../eventio-list-view-selector'
import EventioLoadingIndicator from '../eventio-loading-indicator'
import { DATA } from '../../constants'

// @styles
import './styles.scss'

class EventioEventsList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isGridView: true
        }

        this._onViewChange = this._onViewChange.bind(this)
    }

    componentDidMount() {
        this.props.fetchEvents()
    }

    _onViewChange(selectedViewId) {
        this.setState({
            isGridView: selectedViewId === DATA.VIEW_SELECTORS.grid
        })
    }

    renderEventsList() {
        const { isGridView } = this.state
        const {
            events,
            onEditEvent,
            onJoinEvent,
            onLeaveEvent
        } = this.props


        if (events.get('isFetchingList')) {
            return (
                <EventioLoadingIndicator
                    className="eventio-events-list__loading-indicator"
                    colorGray
                />
            )
        }

        return events
            .get('list')
            .reduce((mappedEvents, event) =>
                mappedEvents.concat([(
                    <EventioEvent
                        key={event.get('id')}
                        event={event}
                        isGridView={isGridView}
                        onEditClicked={onEditEvent}
                        onJoinClicked={onJoinEvent}
                        onLeaveClicked={onLeaveEvent}
                    />
                )])
            , [])
    }

    render() {
        const { _onViewChange } = this
        const { isGridView } = this.state
        const { children } = this.props

        return (
            <div className="eventio-events-list">
                <div className="eventio-events-list__header">
                    <div>
                        {children}
                    </div>
                    <EventioListViewSelector
                        onChange={_onViewChange}
                        defaultView={DATA.VIEW_SELECTORS.grid}
                    />
                </div>
                <div
                    className={bem(
                        'eventio-events-list__events',
                        {
                            grid: isGridView,
                            list: !isGridView
                        }
                    )}
                >
                    {this.renderEventsList()}
                </div>
            </div>
        )
    }
}

EventioEventsList.propTypes = {
    children: PropTypes.node.isRequired,
    events: ImmutablePropTypes.contains({
        isFetchingList: PropTypes.bool.isRequired,
        list: ImmutablePropTypes.list.isRequired
    }).isRequired,
    fetchEvents: PropTypes.func.isRequired,
    onEditEvent: PropTypes.func.isRequired,
    onJoinEvent: PropTypes.func.isRequired,
    onLeaveEvent: PropTypes.func.isRequired
}

export default EventioEventsList
