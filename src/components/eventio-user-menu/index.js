// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import {
    AccessibleFakeButton,
    Avatar,
    DropdownMenu,
    FontIcon,
    IconSeparator
} from 'react-md'

// @styles
import './styles.scss'

const EventioUserMenu = ({
    firstName,
    lastName,
    onLogoutSession,
    onOpenProfile
}) => {
    const initials = [
        firstName.charAt(0),
        lastName.charAt(0)
    ].join('')

    const fullName = `${firstName} ${lastName}`

    return (
        <DropdownMenu
            className="eventio-user-menu"
            id="eventio-user-menu"
            menuItems={[{
                onClick: onOpenProfile,
                primaryText: 'Profile'
            }, {
                onClick: onLogoutSession,
                primaryText: 'Log out'
            }]}
            listClassName="eventio-user-menu__list"
            sameWidth
        >
            <AccessibleFakeButton
                component={IconSeparator}
                iconBefore
                label={
                    <IconSeparator
                        className="eventio-user-menu__user-name"
                        label={fullName}
                    >
                        <FontIcon className="eventio-user-menu__arrow">arrow_drop_down</FontIcon>
                    </IconSeparator>
                }
            >
                <Avatar className="eventio-user-menu__avatar">
                    {initials}
                </Avatar>
            </AccessibleFakeButton>
        </DropdownMenu>
    )
}

EventioUserMenu.propTypes = {
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    onLogoutSession: PropTypes.func.isRequired,
    onOpenProfile: PropTypes.func.isRequired
}

export default EventioUserMenu
