// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import { CircularProgress, bem } from 'react-md'
import classNames from 'classnames'

// @styles
import './styles.scss'

const EventioLoadingIndicator = ({ colorGray, className }) =>
    (
        <CircularProgress
            id="eventio-loading-indicator"
            className={
                classNames(
                    bem('eventio-loading-indicator', { gray: colorGray }),
                    className
                )
            }
        />
    )

EventioLoadingIndicator.propTypes = {
    clasName: PropTypes.string,
    colorGray: PropTypes.bool
}

EventioLoadingIndicator.defaultProps = {
    clasName: '',
    colorGray: false
}

export default EventioLoadingIndicator
