// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { DatePicker } from 'react-md';

// @styles
import './styles.scss'

const EvntioDatePicker = ({ className, ...props }) =>
    (
        <DatePicker
            className={classNames('eventio-date-picker', className)}
            {...props}
        />
    )

EvntioDatePicker.propTypes = { className: PropTypes.string }

EvntioDatePicker.defaultProps = { className: '' }

export default EvntioDatePicker
