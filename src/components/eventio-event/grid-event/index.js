// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import {
    Card,
    CardActions,
    CardText,
    CardTitle,
    FontIcon
} from 'react-md'

// @components
import EventioButton from '../../eventio-button'

// @libraries
import { formatDate } from '../../../utils'

// @styles
import './styles.scss'

const GridEvent = ({
    button,
    className,
    event,
    isButtonLoading
}) => {
    const ownerFullName = [
        event.getIn(['owner', 'firstName']),
        event.getIn(['owner', 'lastName'])
    ].join(' ')
    const attendeesCount = `${event.get('attendees').size} of ${event.get('capacity')}`

    return (
        <Card className={classNames('grid-event', className)}>
            <span className="grid-event__date">
                {formatDate(event.get('startsAt'))}
            </span>
            <Link to={`/events/${event.get('id')}`}>
                <CardTitle
                    className="grid-event__title"
                    subtitle={ownerFullName}
                    title={event.get('title')}
                />
            </Link>
            <CardText className="grid-event__description">
                <p className="grid-event__description-text">
                    {event.get('description')}
                </p>
            </CardText>
            <CardActions className="grid-event__footer">
                <div className="grid-event__count">
                    <FontIcon>person</FontIcon>
                    <span>{attendeesCount}</span>
                </div>
                <EventioButton
                    className="grid-event__button"
                    loading={isButtonLoading}
                    onClick={() => button.onClick(event.get('id'))}
                    primary={!event.get('isOwner')}
                    secondary={event.get('isAttendee')}
                >
                    {button.text}
                </EventioButton>
            </CardActions>
        </Card>
    )
}

GridEvent.propTypes = {
    button: PropTypes.shape({
        onClick: PropTypes.func.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired,
    className: PropTypes.string,
    event: ImmutablePropTypes.contains({
        attendees: ImmutablePropTypes.list.isRequired,
        capacity: PropTypes.number.isRequired,
        description: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
        owner: ImmutablePropTypes.contains({
            firstName: PropTypes.string.isRequired,
            lastName: PropTypes.string.isRequired
        }),
        startsAt: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired
    }).isRequired,
    isButtonLoading: PropTypes.bool.isRequired
}

GridEvent.defaultProps = {
    className: ''
}

export default GridEvent
