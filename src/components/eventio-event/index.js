// @vendors
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'

// @components
import GridEvent from './grid-event'
import ListEvent from './list-event'
import { DATA } from '../../constants'

class EventioEvent extends PureComponent {
    constructor(props) {
        super(props)

        this.state = { isButtonLoading: false }
        this._callApiOnClick = this._callApiOnClick.bind(this)
        this._generateButtonConfig = this._generateButtonConfig.bind(this)
    }

    _callApiOnClick(callback, eventId) {
        this.setState(
            { isButtonLoading: true },
            () => callback(eventId)
        )
    }

    _generateButtonConfig() {
        const { _callApiOnClick } = this
        const {
            event,
            onEditClicked,
            onJoinClicked,
            onLeaveClicked
        } = this.props

        if (event.get('isOwner')) {
            return {
                onClick: (eventId) => _callApiOnClick(onEditClicked, eventId),
                text: DATA.EVENT_BUTTON_MESSAGE.owner
            }
        }

        return event.get('isAttendee')
            ?  {
                onClick: (eventId) => _callApiOnClick(onLeaveClicked, eventId),
                text: DATA.EVENT_BUTTON_MESSAGE.attendee
            }
            : {
                onClick: (eventId) => _callApiOnClick(onJoinClicked, eventId),
                text: DATA.EVENT_BUTTON_MESSAGE.unregistered
            }
    }

    render() {
        const { _generateButtonConfig } = this
        const { isButtonLoading } = this.state
        const {
            className,
            event,
            isGridView
        } = this.props

        const props = {
            button: _generateButtonConfig(),
            className,
            event,
            isButtonLoading
        }

        return isGridView
            ? <GridEvent {...props} />
            : <ListEvent {...props} />
    }
}

EventioEvent.propTypes = {
    className: PropTypes.string,
    event: ImmutablePropTypes.map.isRequired,
    isGridView: PropTypes.bool.isRequired,
    onEditClicked: PropTypes.func.isRequired,
    onJoinClicked: PropTypes.func.isRequired,
    onLeaveClicked: PropTypes.func.isRequired,
}

EventioEvent.defaultProps = {
    className: ''
}

export default EventioEvent
