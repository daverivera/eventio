// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import classNames from 'classnames'
import { Paper } from 'react-md'
import { Link } from 'react-router-dom'

// @components
import EventioButton from '../../eventio-button'

// @libraries
import { formatDate } from '../../../utils'

// @styles
import './styles.scss'

const ListEvent = ({
    button,
    className,
    event,
    isButtonLoading
}) => {
    const ownerFullName = [
        event.getIn(['owner', 'firstName']),
        event.getIn(['owner', 'lastName'])
    ].join(' ')
    const attendeesCount = `${event.get('attendees').size} of ${event.get('capacity')}`

    return (
        <Paper className={classNames('list-event', className)}>
            <h2 className="list-event__title">
                <Link to={`/events/${event.get('id')}`}>
                    {event.get('title')}
                </Link>
            </h2>
            <p className="list-event__description">
                {event.get('description')}
            </p>
            <span className="list-event__owner">{ownerFullName}</span>
            <span className="list-event__date">
                {formatDate(event.get('startsAt'))}
            </span>
            <span className="list-event__count">
                {attendeesCount}
            </span>
            <EventioButton
                className="list-event__button"
                loading={isButtonLoading}
                onClick={() => button.onClick(event.get('id'))}
                primary={!event.get('isOwner')}
                secondary={event.get('isAttendee')}
            >
                {button.text}
            </EventioButton>
        </Paper>
    )
}

ListEvent.propTypes = {
    button: PropTypes.shape({
        onClick: PropTypes.func.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired,
    className: PropTypes.string,
    event: ImmutablePropTypes.contains({
        attendees: ImmutablePropTypes.list.isRequired,
        capacity: PropTypes.number.isRequired,
        description: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
        owner: ImmutablePropTypes.contains({
            firstName: PropTypes.string.isRequired,
            lastName: PropTypes.string.isRequired
        }),
        startsAt: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired
    }).isRequired,
    isButtonLoading: PropTypes.bool.isRequired
}

ListEvent.defaultProps = {
    className: ''
}

export default ListEvent
