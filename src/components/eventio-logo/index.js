// @vendors
import React from 'react'

// @styles
import './styles.scss'

const EventioLogo = () =>
    (
        <h1 className="eventio-logo">
            E.
        </h1>
    )

export default EventioLogo
