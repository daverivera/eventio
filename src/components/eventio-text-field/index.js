// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { TextField } from 'react-md'

// @styles
import './styles.scss'

const EventioTextField = ({ className, ...props }) =>
    (
        <TextField
            className={classNames('eventio-text-field', className)}
            {...props}
        />
    )

EventioTextField.propTypes = { className: PropTypes.string }

EventioTextField.defaultProps = { className: '' }

export default EventioTextField
