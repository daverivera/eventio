// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import { SelectField } from 'react-md'

// @styles
import './styles.scss'

const EventioDropdown = ({
    defaultValue,
    items,
    onChange,
    value
}) =>
    (
        <SelectField
            className="eventio-dropdown"
            defaultValue={defaultValue}
            id="eventio-dropdown"
            menuItems={items}
            onChange={onChange}
            value={value}
        />
    )

EventioDropdown.propTypes = {
    defaultValue: PropTypes.number.isRequired,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            value: PropTypes.number.isRequired
        }).isRequired
    ).isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.number.isRequired
}

export default EventioDropdown
