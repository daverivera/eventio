// @vendors
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import classNames from 'classnames'
import { Paper } from 'react-md'

// @components
import EventioAttendee from './eventio-attendee'

// @styles
import './styles.scss';

class EventioAttendees extends PureComponent {
    renderAttendeesList() {
        const { attendees, userId } = this.props
        return attendees.reduce((mappedAttendees, attendee) =>
            mappedAttendees.concat([(
                <EventioAttendee
                    key={attendee.get('id')}
                    name={`${attendee.get('firstName')} ${attendee.get('lastName')}`}
                    isUser={userId === attendee.get('id')}
                />
            )])
        , [])
    }

    render() {
        const { className } = this.props
        return (
            <Paper className={classNames('eventio-attendees', className)}>
                <h2 className="eventio-attendees__title">Attendees</h2>
                {this.renderAttendeesList()}
            </Paper>
        )
    }
}

EventioAttendees.propTypes = {
    attendees: ImmutablePropTypes.listOf(
        ImmutablePropTypes.contains({
            firstName: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            lastName: PropTypes.string.isRequired
        })
    ).isRequired,
    className: PropTypes.string,
    userId: PropTypes.string.isRequired
}

EventioAttendees.defaultProps = {
    className: ''
}

export default EventioAttendees
