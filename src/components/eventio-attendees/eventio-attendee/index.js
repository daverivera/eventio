// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import { Chip, bem } from 'react-md'

// @styles
import './styles.scss';

const EventioAttendee = ({ isUser, name }) => {
    const label = isUser ? 'You' : name

    return (
        <Chip
            className={bem(
                'eventio-attendee',
                { user: isUser }
            )}
            label={label}
        />
    )
}

EventioAttendee.propTypes = {
    isUser: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired
}

export default EventioAttendee
