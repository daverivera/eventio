// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { Button } from 'react-md'

// @components
import EventioLoadingIndicator from '../eventio-loading-indicator'

// @styles
import './styles.scss'

const EventioButton = ({
    children,
    className,
    disabled,
    loading,
    onClick,
    primary,
    secondary
}) => {
    const clickEvent = !loading ? onClick : undefined

    return (
        <Button
            className={classNames('eventio-button', { 'eventio-button--disabled': disabled }, className)}
            disabled={disabled}
            flat
            onClick={clickEvent}
            primary={primary}
            secondary={secondary}
            swapTheming
        >
            {
                loading
                    ?  <EventioLoadingIndicator />
                    : children
            }
        </Button>
    )
}

EventioButton.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
    primary: PropTypes.bool,
    secondary: PropTypes.bool
}

EventioButton.defaultProps = {
    className: '',
    disabled: false,
    loading: false,
    primary: false,
    secondary: false
}

export default EventioButton
