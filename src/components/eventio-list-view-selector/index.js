// @vendors
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bem, FontIcon } from 'react-md'

// @components
import { DATA } from '../../constants'

// @styles
import './styles.scss'

class EventioListViewSelector extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selectedViewId: props.defaultView
        }

        this._onViewChange = this._onViewChange.bind(this)
    }

    _onViewChange(selectedViewId) {
        const { onChange } = this.props
        this.setState(
            { selectedViewId },
            () => onChange(selectedViewId)
        )
    }

    render() {
        const { selectedViewId } = this.state
        const { _onViewChange } = this
        return (
            <div className="eventio-list-view-selector">
                <FontIcon
                    className={bem(
                        'eventio-list-view-selector',
                        'grid-icon',
                        { active: selectedViewId === DATA.VIEW_SELECTORS.grid }
                    )}
                    onClick={() => _onViewChange(DATA.VIEW_SELECTORS.grid)}
                >
                    view_module
                </FontIcon>
                <FontIcon
                    className={bem(
                        'eventio-list-view-selector',
                        'list-icon',
                        { active: selectedViewId === DATA.VIEW_SELECTORS.list }
                    )}
                    onClick={() => _onViewChange(DATA.VIEW_SELECTORS.list)}
                >
                    view_stream
                </FontIcon>
            </div>
        )
    }
}

EventioListViewSelector.propTypes = {
    defaultView: PropTypes.oneOf([
        DATA.VIEW_SELECTORS.grid,
        DATA.VIEW_SELECTORS.list
    ]).isRequired,
    onChange: PropTypes.func.isRequired
}

export default EventioListViewSelector
