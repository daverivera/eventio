// @vendors
import React from 'react'
import ReactDOM from 'react-dom'
import WebFontLoader from 'webfontloader'

// Initialize Axios default config
import './http/api'

// @components
import Eventio from './eventio'
import registerServiceWorker from './registerServiceWorker'

// @styles
import './styles.scss'

WebFontLoader.load({
    google: {
        families: ['Roboto:300,400,500,700', 'Material Icons'],
    },
})

ReactDOM.render(<Eventio />, document.getElementById('root'))
registerServiceWorker()
