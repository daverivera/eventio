// @vendors
import moment from 'moment'

// @libraies
import { DATA } from '../constants'

export const formatDate = (date, format=DATA.EVENT_DATE_FORMAT) => {
    moment.locale()
    return moment(date).format(format)
}

export const filterEventsByDate = (selectedFilter, events) => {
    moment.locale()
    const now = moment()
    const filterMethod = selectedFilter === DATA.EVENT_CLASSIFICATIONS.FUTURE.value
        ? date => now < date
        : date => now > date

    return events.filter(event => filterMethod(moment(event.get('startsAt'))))
}

export const timeDateToIsoDatetime = (date, time) => {
    moment.locale()
    return moment(`${date} ${time}`, 'MM/DD/YYYY HH:mm A').toISOString()
}

export const getLocaleDate = date => {
    moment.locale()
    return moment(date).toDate()
}

export const getDateFromStringDate = date => {
    moment.locale()
    return moment(date).format('MM/DD/YYYY')
}
