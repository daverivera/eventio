// @vendors
import React, { Fragment } from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'

// @components
import store, { history } from './store'
import Routes from './routes'

const Eventio = () =>
    (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Fragment>
                    <Routes />
                </Fragment>
            </ConnectedRouter>
        </Provider>
    )

export default Eventio
