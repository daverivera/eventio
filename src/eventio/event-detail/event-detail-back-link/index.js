// @vendors
import React from 'react'
import { Link } from 'react-router-dom'
import { FontIcon } from 'react-md'

// @styles
import './styles.scss'

const EventDetailBackLink = () =>
    (
        <div className="event-detail-back-link">
            <Link to="/events" className="event-detail-back-link__hyperlink">
                <FontIcon className="event-detail-back-link__arrow">
                    arrow_back
                </FontIcon>
                <span className="event-detail-back-link__title">
                    Back to events
                </span>
            </Link>
        </div>
    )

export default EventDetailBackLink
