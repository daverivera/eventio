// @vendors
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import classNames from 'classnames'
import {
    Paper,
} from 'react-md'

// @components
import EventioTextField from '../../../components/eventio-text-field'
import EventioTimePicker from '../../../components/eventio-time-picker'
import EventioDatePicker from '../../../components/eventio-date-picker'
import { MESSAGES } from '../../../constants'
import { getDateFromStringDate, getLocaleDate } from '../../../utils'

// @styles
import './styles.scss'

class EventDetailEdit extends Component {
    constructor(props) {
        super(props)

        const { event } = props
        const startsAt = event.get('startsAt')
        const time = getLocaleDate(startsAt)
        const date = getDateFromStringDate(startsAt)

        this.state = {
            capacity: event.get('capacity'),
            date,
            description: event.get('description'),
            time,
            title: event.get('title')
        }

        this._updateField = this._updateField.bind(this)
    }

    _updateField(field, value) {
        const { onChange } = this.props
        const form = Object.assign({}, this.state, { [field]: value })

        this.setState(
            { [field]: value },
            () => onChange(form)
        )
    }

    render() {
        const { _updateField } = this
        const { className } = this.props
        const {
            capacity,
            date,
            description,
            time,
            title
        } = this.state

        return (
            <Paper className={classNames('event-detail-edit', className)}>
                <div className="event-detail-edit__form">
                    <EventioDatePicker
                        errorText={MESSAGES.CREATE_EVENT.date.empty}
                        id="event-detail-edit-date"
                        label="Date"
                        onChange={(value) => _updateField('date', value)}
                        placeholder="Date"
                        required
                        value={date}
                    />
                    <EventioTimePicker
                        errorText={MESSAGES.CREATE_EVENT.time.empty}
                        id="event-detail-edit-time"
                        label="Time"
                        onChange={(value) => _updateField('time', value)}
                        placeholder="Time"
                        required
                        value={time}
                    />
                    <EventioTextField
                        errorText={MESSAGES.CREATE_EVENT.title.empty}
                        id="event-detail-edit-title"
                        label="Title"
                        onChange={(value) => _updateField('title', value)}
                        placeholder="Title"
                        required
                        value={title}
                    />
                    <EventioTextField
                        errorText={MESSAGES.CREATE_EVENT.description.empty}
                        id="event-detail-edit-description"
                        label="Description"
                        onChange={(value) => _updateField('description', value)}
                        placeholder="Description"
                        required
                        value={description}
                    />
                    <EventioTextField
                        errorText={MESSAGES.CREATE_EVENT.capacity.empty}
                        id="event-detail-edit-capacity"
                        label="Capacity"
                        onChange={(value) => _updateField('capacity', value)}
                        placeholder="Capacity"
                        required
                        type="number"
                        value={capacity}
                    />
                </div>
            </Paper>
        )
    }
}

EventDetailEdit.propTypes = {
    className: PropTypes.string,
    event: ImmutablePropTypes.contains({
        capacity: PropTypes.number.isRequired,
        description: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
        startsAt: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired
    }).isRequired,
    onDeleteEvent: PropTypes.func.isRequired
}

EventDetailEdit.defaultProps = {
    className: ''
}

export default EventDetailEdit
