// @vendors
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import { FontIcon } from 'react-md'

// @components
import EventDetailBackLink from './event-detail-back-link'
import EventDetailEdit from './event-detail-edit'
import EventioAttendees from '../../components/eventio-attendees'
import EventioCircularButton from '../../components/eventio-circular-button'
import EventioCreateEvent from '../../components/eventio-create-event'
import EventioEvent from '../../components/eventio-event'
import EventioHeader from '../containers/header-container'
import EventioLoadingIndicator from '../../components/eventio-loading-indicator'

// @styles
import './styles.scss';

class EventDetail extends Component {
    constructor(props) {
        super(props)

        this.state = {
            form: {
                capacity: '',
                date: '',
                description: '',
                time: '',
                title: ''
            },
            isButtonDisabled: false
        }

        this._formChange = this._formChange.bind(this)
        this._onUpdateEvent = this._onUpdateEvent.bind(this)
    }

    componentDidMount() {
        const { fetchEventDetail, match } = this.props
        fetchEventDetail(match.params.id)
    }

    _formChange(form) {
        const isButtonDisabled = !!Object
            .values(form)
            .filter(field => !field)
            .length

        this.setState({ form, isButtonDisabled })
    }

    _onUpdateEvent() {
        const { onEditClicked, match } = this.props
        const { form } = this.state
        onEditClicked(match.params.id, form)
    }

    renderCircularButton() {
        const { _onUpdateEvent } = this
        const { eventDetail } = this.props
        const { isButtonDisabled } = this.state

        return eventDetail.getIn(['event', 'isOwner'])
            ? <EventioCircularButton
                primary={false}
                onClick={_onUpdateEvent}
                disabled={isButtonDisabled}
            />
            : <EventioCreateEvent />
    }

    renderDeleteButton() {
        const { eventDetail } = this.props

        return eventDetail.getIn(['event', 'isOwner'])
            ? (
                <div className="event-detail__delete-container">
                    <FontIcon className="event-detail__delete-icon">delete</FontIcon>
                    <span className="event-detail__delete-text">
                        DELETE EVENT
                    </span>
                </div>
            )
            : null;
    }

    renderEvent() {
        const { _formChange } = this
        const {
            eventDetail,
            onDeleteEvent,
            onJoinEvent,
            onLeaveEvent
        } = this.props

        return eventDetail.getIn(['event', 'isOwner'])
            ? (
                <EventDetailEdit
                    className="event-detail__event"
                    event={eventDetail.get('event')}
                    onDeleteEvent={onDeleteEvent}
                    onChange={_formChange}
                />
            )
            : (
                <EventioEvent
                    className="event-detail__event"
                    event={eventDetail.get('event')}
                    isGridView
                    onEditClicked={() => {}}
                    onJoinClicked={onJoinEvent}
                    onLeaveClicked={onLeaveEvent}
                />
            )
    }

    render() {
        const {
            eventDetail,
            userId
        } = this.props

        if (eventDetail.get('isFetchingEvent')) {
            return <EventioLoadingIndicator colorGray />
        }

        return (
            <Fragment>
                <EventioHeader />
                <EventDetailBackLink />
                <div className="event-detail">
                    <div className="event-detail__header">
                        <span className="event-detail__id-title">
                            DETAIL EVENT: #{eventDetail.getIn(['event', 'id'])}
                        </span>
                        {this.renderDeleteButton()}
                    </div>
                    <div className="event-detail__content">
                        {this.renderEvent()}
                        <EventioAttendees
                            className="event-detail__attendees"
                            attendees={eventDetail.getIn(['event', 'attendees'])}
                            userId={userId}
                        />
                    </div>
                    <div className="event-detail__footer">
                        {this.renderCircularButton()}
                    </div>
                </div>
            </Fragment>
        )
    }
}

EventDetail.propTypes = {
    eventDetail: ImmutablePropTypes.contains({
        event: ImmutablePropTypes.map.isRequired,
        isFetchingEvent: PropTypes.bool.isRequired
    }).isRequired,
    fetchEventDetail: PropTypes.func.isRequired,
    match: PropTypes.shape({
        params: PropTypes.shape({
            id: PropTypes.string.isRequired
        }).isRequired
    }).isRequired,
    onDeleteEvent: PropTypes.func.isRequired,
    onEditClicked: PropTypes.func.isRequired,
    onJoinEvent: PropTypes.func.isRequired,
    onLeaveEvent: PropTypes.func.isRequired,
    userId: PropTypes.string.isRequired
}

export default EventDetail
