// @vendors
import { fromJS } from 'immutable';

// @actions
import {
    EVENT_FETCH_DETAIL,
    EVENT_FETCH_DETAIL_REQUEST
} from '../actions/events'

const initialState = fromJS({
    event: {},
    isFetchingEvent: true
})
export const eventDetailReducer = (state = initialState, action) => {
    switch(action.type) {
        case EVENT_FETCH_DETAIL:
            return state.merge({
                event: action.payload,
                isFetchingEvent: false
            })
        case EVENT_FETCH_DETAIL_REQUEST:
            return state.merge({
                isFetchingEvent: true
            })
        default:
            return state
    }
}
