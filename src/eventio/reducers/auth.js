// @vendors
import { fromJS } from 'immutable';

// @actions
import {
    AUTH_LOG_IN_FAILED,
    AUTH_LOG_IN_REQUEST,
    AUTH_LOG_IN_SUCCESSFUL,
    AUTH_LOG_OUT
} from '../actions/auth'

const initialState = fromJS({
    authToken: undefined,
    hasLoginFailed: false,
    isLogin: false,
    user: {
        id: '',
        firstName: '',
        lastName: '',
        email: ''
    }
})
export const authReducer = (state = initialState, action) => {
    switch(action.type) {
        case AUTH_LOG_IN_FAILED:
            return state.merge({
                hasLoginFailed: true,
                isLogin: false
            })
        case AUTH_LOG_IN_REQUEST:
            return state.merge({ isLogin: true })
        case AUTH_LOG_IN_SUCCESSFUL:
            const { authToken, user } = action.payload
            return state.merge({
                authToken,
                hasLoginFailed: false,
                isLogin: false,
                user
            })
        case AUTH_LOG_OUT:
            return initialState
        default:
            return state
    }
}
