// @vendors
import { fromJS } from 'immutable';

// @actions
import {
    EVENTS_FILTER_BY_DATE,
    EVENTS_GET_LIST,
    EVENTS_GET_LIST_REQUEST
} from '../actions/events'

const initialState = fromJS({
    isFetchingList: false,
    list: [],
    original: []
})
export const eventsReducer = (state = initialState, action) => {
    switch(action.type) {
        case EVENTS_FILTER_BY_DATE:
            return state.merge({
                list: action.payload
            })
        case EVENTS_GET_LIST:
            const events = fromJS(action.payload)
            return state.merge({
                isFetchingList: false,
                list: events,
                original: events
            })
        case EVENTS_GET_LIST_REQUEST:
            return state.merge({ isFetchingList: true })
        default:
            return state
    }
}
