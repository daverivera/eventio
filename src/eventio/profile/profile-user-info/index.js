// @vendors
import React from 'react'
import PropTypes from 'prop-types'
import { Avatar, Paper } from 'react-md'

// @components

// @styles
import './styles.scss'

const ProfileUserInfo = ({ email, firstName, lastName }) => {
    const fullName = `${firstName} ${lastName}`
    const initials = [
        firstName.charAt(0),
        lastName.charAt(0)
    ].join('')

    return (
        <Paper className="profile-user-info">
            <Avatar className="profile-user-info__avatar">
                {initials}
            </Avatar>
            <h2 className="profile-user-info__user-name">{fullName}</h2>
            <h3 className="profile-user-info__user-email">{email}</h3>
        </Paper>
    )
}

ProfileUserInfo.propTypes = {
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired
}

export default ProfileUserInfo
