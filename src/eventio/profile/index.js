// @vendors
import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'

// @components
import EventioHeader from '../containers/header-container'
import EventioEventsList from '../containers/events-list-container'
import ProfileUserInfo from './profile-user-info'

// @styles
import './styles.scss'

const Profile = ({ user }) => {
    return (
        <Fragment>
            <EventioHeader />
            <div className="profile">
                <ProfileUserInfo
                    email={user.get('email')}
                    firstName={user.get('firstName')}
                    lastName={user.get('lastName')}
                />
                <div className="profile__events-list">
                    <EventioEventsList>
                        <h2 className="profile__events-list-title">My Events</h2>
                    </EventioEventsList>
                </div>
            </div>
        </Fragment>
    )
}

Profile.propTypes = {
    user: ImmutablePropTypes.contains({
        email: PropTypes.string.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired
    })
}

export default Profile
