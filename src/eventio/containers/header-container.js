// @vendors
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'

// @components
import EventioHeader from '../../components/eventio-header'
import { PAGES } from '../../constants'
import { logout } from '../actions/auth'

const mapStateToProps = ({ auth }) => ({
    user: auth.get('user')
})

const mapDispatchToProps = dispatch => bindActionCreators({
    onLogoutSession: logout,
    onOpenProfile: () => push(PAGES.profile)
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(EventioHeader)
