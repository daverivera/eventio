// @vendors
import { connect } from 'react-redux'
import { push } from 'react-router-redux'

// @components
import EventioEventsList from '../../components/eventio-events-list'
import { PAGES } from '../../constants'
import {
    fetchEvents,
    joinEvent,
    leaveEvent
} from '../actions/events'

const mapStateToProps = ({ events }) => ({
    events
})

const mapDispatchToProps = dispatch => ({
    fetchEvents: () => dispatch(fetchEvents()),
    onEditEvent: eventId => {
        dispatch(push(PAGES.eventDetail(eventId)))
    },
    onJoinEvent: eventId => {
        dispatch(joinEvent(eventId)).then(() => {
            dispatch(fetchEvents())
        })
    },
    onLeaveEvent: eventId => {
        dispatch(leaveEvent(eventId)).then(() => {
            dispatch(fetchEvents())
        })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(EventioEventsList)
