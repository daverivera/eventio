// @vendors
import { connect } from 'react-redux'

// @components
import EventDetail from '../event-detail'
import {
    deleteEvent,
    fetchEventDetail,
    joinEvent,
    leaveEvent,
    updateEvent
} from '../actions/events'

const mapStateToProps = ({ eventDetail, auth }) => ({
    eventDetail,
    userId: auth.getIn(['user', 'id'])
})

const mapDispatchToProps = dispatch => ({
    fetchEventDetail: eventId => dispatch(fetchEventDetail(eventId)),
    onDeleteEvent: eventId => {
        dispatch(deleteEvent(eventId)).then(() => {
            dispatch(fetchEventDetail(eventId))
        })
    },
    onEditClicked: (eventId, event) => {
        console.log(eventId, event)
        dispatch(updateEvent(eventId, event)).then(() => {
            dispatch(fetchEventDetail(eventId))
        })
    },
    onJoinEvent: eventId => {
        dispatch(joinEvent(eventId)).then(() => {
            dispatch(fetchEventDetail(eventId))
        })
    },
    onLeaveEvent: eventId => {
        dispatch(leaveEvent(eventId)).then(() => {
            dispatch(fetchEventDetail(eventId))
        })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(EventDetail)
