// @vendors
import { connect } from 'react-redux'

// @components
import EventioCreateEvent from '../../components/eventio-create-event'
import { createEvent, fetchEvents } from '../actions/events'

const mapStateToProps = () => ({ })

const mapDispatchToProps = dispatch => ({
    onCreateEvent: event => {
        dispatch(createEvent(event)).then(() => {
            dispatch(fetchEvents())
        })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(EventioCreateEvent)
