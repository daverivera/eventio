// @vendors
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// @components
import Login from '../login'
import { login } from '../actions/auth'

const mapStateToProps = ({ auth }) => ({
    hasLoginFailed: auth.get('hasLoginFailed'),
    isLogin: auth.get('isLogin')
})

const mapDispatchToProps = dispatch => bindActionCreators({
    onLogin: login
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Login)
