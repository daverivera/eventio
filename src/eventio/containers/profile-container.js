// @vendors
import { connect } from 'react-redux'

// @components
import Profile from '../profile'

const mapStateToProps = ({ auth }) => ({
    user: auth.get('user')
})

export default connect(mapStateToProps)(Profile)
