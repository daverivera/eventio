// @vendors
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// @components
import Events from '../events'
import { filterByDate } from '../actions/events'

const mapStateToProps = ({ events }) => ({
    events
})

const mapDispatchToProps = dispatch => bindActionCreators({
    filterEventsByDate: filterByDate
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Events)
