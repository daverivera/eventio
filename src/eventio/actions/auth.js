// @vendors
import axios from 'axios'
import { push } from 'react-router-redux'

// @libraries
import { ENDPOINTS, PAGES } from '../../constants'

export const AUTH_LOG_IN_FAILED = 'AUTH_LOG_IN_FAILED'
export const AUTH_LOG_IN_REQUEST = 'AUTH_LOG_IN_REQUEST'
export const AUTH_LOG_IN_SUCCESSFUL = 'AUTH_LOG_IN_SUCCESSFUL'
export const login = (email, password) =>
    dispatch => {
        dispatch({ type: AUTH_LOG_IN_REQUEST })
        return axios
            .post(ENDPOINTS.AUTH.login, { email, password })
            .then(({ data, headers }) =>  {
                dispatch({
                    payload: {
                        authToken: headers.authorization,
                        user: data
                    },
                    type: AUTH_LOG_IN_SUCCESSFUL
                })
                dispatch(push(PAGES.events))
            })
            .catch(() => dispatch({ type: AUTH_LOG_IN_FAILED }))
    }

export const AUTH_LOG_OUT = 'AUTH_LOG_OUT'
export const logout = () =>
    dispatch => {
        dispatch(push(PAGES.login))
        return ({ type: AUTH_LOG_OUT })
    }
