// @vendors
import axios from 'axios'

// @libraries
import { filterEventsByDate, timeDateToIsoDatetime } from '../../utils'
import { DATA, ENDPOINTS } from '../../constants'

const additionalEventProps = userId => event => {
    const isOwner = event.owner.id === userId
    const isAttendee = !isOwner && !!event.attendees.find(({ id }) => id === userId)

    return { ...event, isOwner, isAttendee }
}

export const EVENTS_GET_LIST = 'EVENTS_GET_LIST'
export const EVENTS_GET_LIST_REQUEST = 'EVENTS_GET_LIST_REQUEST'
export const fetchEvents = () =>
    (dispatch, getState) => {
        dispatch({ type: EVENTS_GET_LIST_REQUEST })
        return axios
            .get(ENDPOINTS.EVENTS.listAllEvents)
            .then(({ data }) => {
                const userId = getState().auth.getIn(['user', 'id'])
                const events = data.map(additionalEventProps(userId))

                return dispatch({
                    payload: events,
                    type: EVENTS_GET_LIST
                })
            })
    }

export const EVENTS_FILTER_BY_DATE = 'EVENTS_FILTER_BY_DATE'
export const filterByDate = (selectedFilter) =>
    (dispatch, getState) => {
        const { events } = getState()
        const { EVENT_CLASSIFICATIONS } = DATA
        const filteredEvents = selectedFilter === EVENT_CLASSIFICATIONS.ALL.value
            ? events.get('original')
            : filterEventsByDate(selectedFilter, events.get('original'))

        dispatch({
            payload: filteredEvents,
            type: EVENTS_FILTER_BY_DATE
        })
    }

export const EVENTS_LEAVE = 'EVENTS_LEAVE'
export const leaveEvent = eventId =>
    (dispatch, getState) => {
        const Authorization = getState().auth.get('authToken')

        return axios({
            method: 'delete',
            url: ENDPOINTS.EVENTS.attendEvent(eventId),
            headers: { Authorization }
        }).then(({ data }) =>
                dispatch({
                    payload: eventId,
                    type: EVENTS_LEAVE
                })
            )
    }

export const EVENTS_JOIN = 'EVENTS_JOIN'
export const joinEvent = eventId =>
    (dispatch, getState) => {
        const Authorization = getState().auth.get('authToken')
        return axios({
            method: 'post',
            url: ENDPOINTS.EVENTS.attendEvent(eventId),
            headers: { Authorization }
        }).then(({ data }) =>
            dispatch({
                payload: eventId,
                type: EVENTS_JOIN
            })
        )
    }

export const EVENT_FETCH_DETAIL = 'EVENT_FETCH_DETAIL'
export const EVENT_FETCH_DETAIL_REQUEST = 'EVENT_FETCH_DETAIL_REQUEST'
export const fetchEventDetail = eventId =>
    (dispatch, getState) => {
        dispatch({ type: EVENT_FETCH_DETAIL_REQUEST })
        return axios
            .get(ENDPOINTS.EVENTS.getSpecificEvent(eventId))
            .then(({ data }) => {
                const userId = getState().auth.getIn(['user', 'id'])

                dispatch({
                    payload: additionalEventProps(userId)(data),
                    type: EVENT_FETCH_DETAIL
                })
            })
    }

export const EVENT_CREATE_EVENT = 'EVENT_CREATE_EVENT'
export const createEvent = ({
    date,
    time,
    ...event
}) =>
    (dispatch, getState) => {
        const formattedEvent = {
            ...event,
            startsAt: timeDateToIsoDatetime(date, time)
        }
        const Authorization = getState().auth.get('authToken')

        return axios({
            method: 'post',
            url: ENDPOINTS.EVENTS.createEvent,
            headers: { Authorization },
            data: formattedEvent
        }).then(() => {
            dispatch({ type: EVENT_CREATE_EVENT })
        })
    }

export const EVENT_DELETE_EVENT = 'EVENT_DELETE_EVENT'
export const deleteEvent = eventId =>
    (dispatch, getState) => {
        const Authorization = getState().auth.get('authToken')
        return axios({
            method: 'delete',
            url: ENDPOINTS.EVENTS.deleteEvent(eventId),
            headers: { Authorization }
        }).then(() => {
            dispatch({ type: EVENT_DELETE_EVENT })
        })
    }

export const EVENT_UPDATE_EVENT =  'EVENT_UPDATE_EVENT'
export const updateEvent = (eventId, eventData) =>
    (dispatch, getState) => {
        const {
            date,
            time,
            ...event
        } = eventData
        const formattedEvent = {
            ...event,
            startsAt: timeDateToIsoDatetime(date, time)
        }

        const Authorization = getState().auth.get('authToken')

        return axios({
            method: 'patch',
            url: ENDPOINTS.EVENTS.updateEvent(eventId),
            headers: { Authorization },
            data: formattedEvent
        }).then(() => {
            dispatch({ type: EVENT_DELETE_EVENT })
        })
    }
