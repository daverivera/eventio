// @vendors
import React from 'react'
import {
    Redirect,
    Route,
    Switch
} from 'react-router'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

// @components
import Events from './containers/events-container'
import EventDetail from './containers/event-detail-container'
import Login from './containers/login-container'
import Profile from './containers/profile-container'

const Routes = ({ isLoggedIn }) => {
    const renderComponent = component =>
        () => isLoggedIn
            ? component
            : <Redirect to="/" />

    return (
        <Switch>
            <Route exact path="/" component={Login}/>
            <Route exact path="/events" render={renderComponent(Events)}/>
            <Route exact path="/events/:id" render={renderComponent(EventDetail)}/>
            <Route exact path="/profile" render={renderComponent(Profile)}/>
        </Switch>
    )
}

const mapStateToProps = ({ auth }) => ({
    isLoggedIn: !!auth.get('authToken')
})

export default withRouter(connect(mapStateToProps)(Routes))

// // import PageNotFound from './common/components/PageNotFound';
// // <Route path="*" component={PageNotFound} />
