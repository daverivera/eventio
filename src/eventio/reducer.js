// @vendors
import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

// @reducers
import { authReducer } from './reducers/auth'
import { eventsReducer } from './reducers/events'
import { eventDetailReducer } from './reducers/event-detail'

export default combineReducers({
    auth: authReducer,
    eventDetail: eventDetailReducer,
    events: eventsReducer,
    routing: routerReducer
})
