// @vendors
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Grid, Cell } from 'react-md'

// @components
import EventioLogo from '../../components/eventio-logo'
import LoginForm from './login-form'
import LoginQuoteImage from './login-quote-image'

// @libraries
import { MESSAGES } from '../../constants'

// @styles
import './styles.scss'

class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            emailErrorMessage: '',
            isEmailMissing: false,
            isPasswordMissing: false,
            password: '',
            passwordErrorMessage: ''
        }

        this._updateEmail = this._updateEmail.bind(this)
        this._updatePassword = this._updatePassword.bind(this)
        this._loginInUser = this._loginInUser.bind(this)
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if(nextProps.hasLoginFailed) {
            this.setState({
                emailErrorMessage: '',
                isEmailMissing: true,
                isPasswordMissing: true,
                passwordErrorMessage: ''
            })
        }
    }

    _updateEmail(email) {
        this.setState({ email })
    }

    _updatePassword(password) {
        this.setState({ password })
    }

    _loginInUser() {
        const { email, password } = this.state
        const { onLogin } = this.props
        const { LOGIN_PAGE } = MESSAGES

        const passwordErrorMessage = !password ? LOGIN_PAGE.password.empty : ''
        const emailErrorMessage = !email ? LOGIN_PAGE.email.empty : ''

        return (!email || !password)
            ? this.setState({
                emailErrorMessage,
                isEmailMissing: !email,
                isPasswordMissing: !password,
                passwordErrorMessage
            })
            : onLogin(email, password)
    }

    render() {
        const {
            _updateEmail,
            _updatePassword,
            _loginInUser
        } = this

        const { hasLoginFailed, isLogin } = this.props

        return (
            <Fragment>
                <EventioLogo />
                <Grid noSpacing className="login">
                    <Cell
                        desktopSize={3}
                        phoneHidden
                    >
                        <LoginQuoteImage />
                    </Cell>
                    <Cell
                        desktopSize={9}
                        phoneSize={12}
                        className="login__content"
                    >
                        <div
                            className="login__form"
                        >
                            <div className="login__header">
                                <h2 className="login__title">Sign in to Eventio.</h2>
                                <h3 className="login__subtitle">Enter your details below.</h3>
                                {
                                    hasLoginFailed
                                        ?  (
                                            <span className="login__failed-error-text">
                                                {MESSAGES.LOGIN_PAGE.failedLogin}
                                            </span>
                                        )
                                    : null
                                }
                            </div>
                            <LoginForm
                                {...this.state}
                                isLogin={isLogin}
                                onEmailChange={_updateEmail}
                                onPasswordChange={_updatePassword}
                                onLogin={_loginInUser}
                            />
                        </div>
                    </Cell>
                </Grid>
            </Fragment>
        )
    }
}

Login.propTypes = {
    hasLoginFailed: PropTypes.bool.isRequired,
    isLogin: PropTypes.bool.isRequired,
    onLogin: PropTypes.func.isRequired
}

export default Login
