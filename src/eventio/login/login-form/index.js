// @vendors
import React from 'react'
import PropTypes from 'prop-types'

// @components
import EventioButton from '../../../components/eventio-button'
import EventioTextField from '../../../components/eventio-text-field'

import './styles.scss'

const LoginForm = ({
    email,
    emailErrorMessage,
    isEmailMissing,
    isLogin,
    isPasswordMissing,
    onEmailChange,
    onLogin,
    onPasswordChange,
    password,
    passwordErrorMessage
}) =>
    (
        <div className="login-form">
            <div className="login-form__field-group">
                <EventioTextField
                    error={isEmailMissing}
                    errorText={emailErrorMessage}
                    id="login-form-email"
                    label="Email"
                    onChange={onEmailChange}
                    value={email}
                />
                <EventioTextField
                    error={isPasswordMissing}
                    errorText={passwordErrorMessage}
                    id="login-form-password"
                    label="Password"
                    lineDirection="right"
                    onChange={onPasswordChange}
                    type="password"
                    value={password}
                />
            </div>
            <EventioButton
                primary
                className="login-form__sign-in-button"
                loading={isLogin}
                onClick={onLogin}
            >
                SIGN IN
            </EventioButton>
        </div>
    )

LoginForm.propTypes = {
    email: PropTypes.string.isRequired,
    emailErrorMessage: PropTypes.string.isRequired,
    isEmailMissing: PropTypes.bool.isRequired,
    isLogin: PropTypes.bool.isRequired,
    isPasswordMissing: PropTypes.bool.isRequired,
    onEmailChange: PropTypes.func.isRequired,
    onLogin: PropTypes.func.isRequired,
    onPasswordChange: PropTypes.func.isRequired,
    password: PropTypes.string.isRequired,
    passwordErrorMessage: PropTypes.string.isRequired
}

export default LoginForm
