// @vendors
import React from 'react'
import { FontIcon } from 'react-md'

// @components
import EventioLogo from '../../../components/eventio-logo'

// @styles
import './styles.scss'

const LoginQuoteImage = () =>
    (
        <div className="login-quote-image">
            <EventioLogo />
            <span className="login-quote-image__quote">"Great, Kid. Don't get cocky."</span>
            <FontIcon className="login-quote-image__separator">minimize</FontIcon>
            <span className="login-quote-image__author">Han Solo</span>
        </div>
    )

export default LoginQuoteImage
