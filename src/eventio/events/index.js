// @vendors
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// @components
import ClassificationSelectors from './classification-selectors'
import EventioCreateEvent from '../containers/create-event-container'
import EventioEventsList from '../containers/events-list-container'
import EventioHeader from '../containers/header-container'
import { DATA } from '../../constants'

// @styles
import './styles.scss';

class Events extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selectedClassification: DATA.EVENT_CLASSIFICATIONS.ALL.value
        }

        this._onClassificationChange = this._onClassificationChange.bind(this)
    }

    _onClassificationChange(selectedClassification) {
        const { filterEventsByDate } = this.props
        this.setState(
            { selectedClassification },
            () => filterEventsByDate(selectedClassification)
        )
    }

    render() {
        const { _onClassificationChange } = this
        const { selectedClassification } = this.state

        return (
            <div className="events">
                <EventioHeader />
                <div className="events__content">
                    <EventioEventsList>
                        <ClassificationSelectors
                            onClassificationChange={_onClassificationChange}
                            selectedClassification={selectedClassification}
                        />
                    </EventioEventsList>
                </div>
                <div className="events__footer">
                    <EventioCreateEvent />
                </div>
            </div>
        )
    }
}

Events.propTypes = {
    filterEventsByDate: PropTypes.func.isRequired,
}

export default Events
