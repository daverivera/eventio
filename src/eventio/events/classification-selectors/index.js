// @vendors
import React, { Fragment, PureComponent } from 'react'
import PropTypes from 'prop-types'
import { bem } from 'react-md'

// @components
import EventioDropdown from '../../../components/eventio-dropdown'
import { DATA } from '../../../constants'

// @styles
import './styles.scss'

class ClassificationSelectors extends PureComponent {
    renderDropdownSelector() {
        const { onClassificationChange, selectedClassification } = this.props

        return (
            <div className="classification-list__dropdown">
                <span className="classification-list__dropdown-label">SHOW:</span>
                <EventioDropdown
                    defaultValue={DATA.EVENT_CLASSIFICATIONS.ALL.value}
                    items={Object.values(DATA.EVENT_CLASSIFICATIONS)}
                    onChange={onClassificationChange}
                    value={selectedClassification}
                />
            </div>
        )
    }

    renderListSelector() {
        const {
            onClassificationChange,
            selectedClassification
        } = this.props
        const items  = Object
            .values(DATA.EVENT_CLASSIFICATIONS)
            .map(({ label, value }, index) => (
                <span
                    className={bem(
                        'classification-list',
                        'list-item',
                        { active: selectedClassification === value}
                    )}
                    key={index}
                    onClick={() => onClassificationChange(value)}
                >
                    {label}
                </span>
            ))

        return (
            <div className="classification-list__list">
                {items}
            </div>
        )
    }

    render() {
        return (
            <Fragment>
                {this.renderDropdownSelector()}
                {this.renderListSelector()}
            </Fragment>
        )
    }
}

ClassificationSelectors.propTypes = {
    onClassificationChange: PropTypes.func.isRequired,
    selectedClassification: PropTypes.number.isRequired
}

export default ClassificationSelectors
